<?php

namespace App\Http\Controllers;

use App\Models\Contact;

class ContactsController // extension of base controller class isn't needed as request objects aren't injected/used
{
    public function __invoke(Contact $contacts)
    {
        return view('pages.main',[
            'limit' => $contacts::LIMIT
        ]);
    }
}

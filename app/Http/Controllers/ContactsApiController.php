<?php

namespace App\Http\Controllers;

use App\Http\Requests\ContactRequest;
use App\Models\Contact;

// I usually write my controllers to follow the CRUDy controller design, each method is responsible for a request 'verb'
class ContactsApiController extends Controller
{

    private $contact_model;

    public function __construct(Contact $contact)
    {
        $this->contact_model = $contact;
    }

    public function getContacts()
    {
        return [
            'contacts' => $this->contact_model
                ->setFilters(
                    request()->all()
                )
                ->filter()
                ->limit($this->contact_model::LIMIT)
                ->get(),
            'total_contacts' => $this->contact_model->count()
        ];
    }

    public function updateContact(ContactRequest $request, Contact $contact)
    {
        return $contact->update([
            'first_name' => $request->get('first_name'),
            'last_name' => $request->get('last_name'),
            'email' => $request->get('email'),
            'phone_number' => $request->get('phone_number'),
        ]);
    }

    public function createContact(ContactRequest $request)
    {
        $contact = $this->contact_model;

        return $contact->fill([
            'first_name' => $request->get('first_name'),
            'last_name' => $request->get('last_name'),
            'email' => $request->get('email'),
            'phone_number' => $request->get('phone_number'),
        ])->save();
    }


}

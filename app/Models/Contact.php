<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    const LIMIT = 5;

    private $filters;

    protected $guarded = [
        'id'
    ];

    public function setFilters(array $filters): self
    {
        $this->filters = $filters;

        return $this;
    }

    /**
     * @param Builder $query
     * @return Builder
     * Placed the logic here instead of in the controller as its the models responsibility to interact with the table data not the controller
     */
    public function scopeFilter(Builder $query): Builder
    {
        return $query->when($this->filters['first_name'] ?? false, function (Builder $when_query) {
            $when_query->where('first_name', 'LIKE', '%' . $this->filters['first_name'] . '%');
        })
            ->when($this->filters['last_name'] ?? false, function (Builder $when_query) {
                $when_query->where('last_name', 'LIKE', '%' . $this->filters['last_name'] . '%');
            })
            ->when($this->filters['phone_number'] ?? false, function (Builder $when_query) {
                $when_query->where('phone_number', 'LIKE', '%' . $this->filters['phone_number'] . '%');
            })
            ->when($this->filters['email_address'] ?? false, function (Builder $when_query) {
                $when_query->where('email_address', 'LIKE', '%' . $this->filters['email_address'] . '%');
            })
            ->when($this->filters['offset'] ?? false, function (Builder $when_query) {
                $when_query->offset($this->filters['offset']);
            });
    }

}

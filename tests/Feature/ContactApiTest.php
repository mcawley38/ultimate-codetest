<?php

namespace Tests\Feature;

use App\Models\Contact;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class ContactApiTest extends TestCase
{
    use RefreshDatabase;

    /**
     * A basic feature test example as a proof of knowledge concept,
     * usually this would have a method for each route and failure states for
     * ones with validation (and failed auth if relevant).
     *
     * @return void
     */
    public function testGetContactRoute()
    {
        $contact = factory(Contact::class)->create();

        $response = $this->get(route('api.contacts.get'));

        $response->assertStatus(200)->assertJson([
            'contacts' => [
                $contact->toArray()
            ],
            //Dependency injection doesn't seem to work here (or automatic dependency injection doesn't at least)
            'total_contacts' => (new Contact())->count()
        ]);
    }
}

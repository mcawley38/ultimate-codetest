<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/contacts', 'ContactsApiController@getContacts')->name('api.contacts.get');
Route::patch('/contacts/{contact}/update', 'ContactsApiController@updateContact')->name('api.contacts.update');
Route::post('/contacts/create', 'ContactsApiController@createContact')->name('api.contacts.create');

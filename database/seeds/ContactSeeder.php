<?php

use Illuminate\Database\Seeder;

class ContactSeeder extends Seeder
{
    const DEFAULT = 1;
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $count = $this->command->ask('How many contacts do you wish to make?');

        $sanitised_count = is_numeric($count) ? (int) $count : self::DEFAULT;

        factory(\App\Models\Contact::class,$sanitised_count)->create();
    }
}
